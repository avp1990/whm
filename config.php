<?php

$v01_pass = '875374f6fda82e2bd56168870a80c968
383f41c9906ca6973ed421907bb6f13d
5dc42717bfc11d6edbd2d1e5e31bb2f5
909fa59c6603b6ce3adaaca907e3522b
dc0b6ae0066a9d750cb356f5e2404975
b3c64755b8510bcb20a8d97db5bbe8f1
34dbee32daffbaddfc3406c9cf0d36ae
f1d357f9f9b3d667dae13750aa5c3524
efa53c1bdb283435e76e3694bdec2105
21c371131602de649f56f3c46d12c8c3
49712a3b27a48bf9cbcc222198005636
3ac910e9ff2283b42e70948222b4737f
99ebb12b3eec7f9620b49a8fbbcdd819
2476af6423484081ac793419316ebb97
372d4b5483e2f49d10969dd9e8210610
5a0850442874274da0765d558bfafcd9
a44f0a4cec3eb69f50dea43fe84583de
ca13fa03f9ac977871dcba104d57e740
4cc7dde56d6de04e99a1ed7bbe20bb5b
8aa3184e515391386eaac033ccaa5741
1266cf44742a7108244eb23f9695f905
071bb98440ea7835b18cd131979a013c
00ff1ef08ed80b05f2d26fd8662c2c36
637714489190a7adeab08e5ce0c6d547
4e7601485d5317173c35c3f8003c2f63
5d87ec214f12335ab9bdf648fe242c66
506946d32ada823eb2de2b4b0a3ab727
cdd697dbc83bc5f7bc20e82a8ee3f4f2
dcc586f71b57e526b0c5096e66e9eb24';

$v03_pass = 'd890599c933b3a05a7dd13a8a9fa1075
5980a7dadbd9c4d83eac1b38a41ecf07
a2d9980b0816b12ca9d0eb0930393381
abdf95f6023ea344dc31c5fb14a521e2
420e65e0e7c7201434e648d74668dee0
b9aba389bc0e512d559a6eb2a091c5ed
1d74eaa3d28be7b4a98a80b6ed4fd298
7959bedf75e8546e4860769d0f2f71a6
ec619e2226f31364d5ce95937cd804f1
f1c7908ca5aa0210686dbab444bef991
9fd314dd416bd642b0f05e8f46f04abc
e8eb400927a30cce87a4eaa6ae85e784
c5dc130e82c9574ce8423d1840605e39
88168ac1b968913a27b8d865d985cef0
e209ec35266f56e9fdfe1cfcfaca7d65
1d19b05dc5e8f854fc03199d86ea990f
e574e727c29db3ab597ff8794603a4be
05d4b314d177bd970c6682f16799cc4e
3e501a898fa2666a71c64fb3c84d47dc
50c4c0cec52dc9186f913b4d8b9eb08a
ab2dbcb7006d7b54245673b1d7d81b3e
ef696e64bdfd2f3fecea24fb01519cd5
8d082768c4606eeb2bf820519086593f
d9c3f4ed8b571d0e996fb62b272a8f21
813872ced743c30209423e4a07a6043b
b00b239213fda428211c2a11249b39a6
d5b84127ba0d0b1e09f062a2de807a66
34260aced702666572a5382af9fc1189
2d427a3e8f4947af89219972b862ada0
c584dd5c2b010c26703290cee58ef7d9';


$v05_pass = '74ef1da2d5c3e8d73277f2ecc154621f
b00db9f868bb6f0e4f282a1b6c01c793
2797500c1fa6aa4e9be9436f8e089b4d
eb34fdd3f3ebeeedad9d206bc148f5bd
9ed7ed97c92a2558b5021ede58f1f019
e9a4ef9ce6ed6fdcb6eb9aeea33652fc
9b21bd68b78a7dfb90b3a2ad440b44ce
9fe3b880ca7a7eccae37e7f65f589691
43e8ab79f199ab00d6c52d56050cbd54
1acb9386f5c158f3d398662f47d13a1c
f9a05580dc3ed4a4fa57bc7ecfbc2af7
ee61589507481e42b7a40e44314c50e8
1349d1cbe4ac1b75f1f562cdc370a29c
2d7e2bb10ece6f68dbe2918694bd32ca
d79db5bdc842c228bd4d071aa7df50cb
ac532b940ed0b2304bce7a60c151a829
e6e56f3fa65a32fa65ffb7b4feea702d
9e57da8514b9099f98879d7eb71fd37a
1867adefc12c35fe02a3de84b2c32ce5
9ad45db0b72bc65da3f234da4ddba567
086b605f22c0af98dadba12e20fce3fc
91ffcc0356c3c5cdb251206285f425f6
8f44cd5c5b4121df1fd645cd9608b6f2
4f27e912d5989cf548368b8da71b0065
052efd637f099b89bcc6bab0eafb4624
8d8252e43ea291b35bcb3e8e20a05797
3c9890596916fc15314dfa6aa7535d0d
3dc7b95fc03940667b4fa5b8d171fa72
75c4c078016c8a71d4142c92397989bd
17910929b234b61aa33735fd2f5085bc';

$v02_pass = decrypt("SrdM3f0z+4hn+VR1SFHPPDyqylMkw2GDK+hGgGs=");


$config = [
    'servers' => [
            'v01' => [
                'id' => '1', 
                'host' => 'https://v01.bizneshost.by:2087',
                'login' => 'root', 
                'pass' => $v01_pass,
                'ip' => '178.124.129.165',
                'type' => 'Cpanel',
            ],

            'v03' => [
                    'id' => '4',
                    'host' => 'https://v03.bizneshost.by:2087',
                    'login' => 'root',
                    'pass' => $v03_pass,
                    'ip' => '178.124.128.199',
                    'type' => 'Cpanel',
            ],

            'v05' => [
                    'id' => '5',
                    'host' => 'https://v05.bizneshost.by:2087',
                    'login' => 'root',
                    'pass' => $v05_pass,
                    'ip' => '178.124.128.203',
                    'type' => 'Cpanel',
            ],

            'v02' => [
                    'id' => '2',
                    'host' => 'v02.bizneshost.by',
                    'login' => 'serveradmin',
                    'pass' => $v02_pass,
                    'ip' => '178.124.129.173',
                    'type' => 'SolidCP',
            ],

    ],

    'exceptions' => [
        'Конструктор.Старт',
        'Конструктор.Профи',
        'VX-1',
        'VX-2',
        'VX-3',
        'VX-4',
    ],
    
    'ommit_if_status_is' => [
        'Cancelled',
        'Terminated',
    ],
    
    'registrants_types' => [
        'Физическое лицо',
        'Индивидуальный предприниматель',
        'Юридическое лицо',
    ],
    
];

return $config;