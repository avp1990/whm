<div class="col-xs-12 main-content">
    
    <div class = 'row'>
        <p class="col-md-8 col-md-offset-5">Сведения о ресурсе:</p>
        <dl class="dl-horizontal dl-horizontal  col-md-8 col-md-offset-3">
            <dt>Доменное имя</dt>
            <dd>{$request.main_domain}</dd>
            <dt>Описание ресурса</dt>
            <dd>{$request['description']}</dd>
            {if $request.domains}
            <dt>Дополнительные домены</dt>
            <dd>{$request.domains}</dd>
            {/if}
        </dl>        
    </div>

    <div class = 'row'>
        <p class="col-md-8 col-md-offset-5">Собственник ресурса</p>
        <dl class="dl-horizontal dl-horizontal  col-md-8 col-md-offset-3">
            <dt>Тип регистранта</dt>
            {if $request.type == '1'}
            <dd>Юридическое лицо</dd>
            {elseif $request.type == '2'}
            <dd>Индивидуальный предприниматель</dd>
            {else}
            <dd>Физическое лицо</dd>
            {/if}
            {if $request.type  == '1'}
            <dt>Наименование Юридического лица</dt>
            <dd>{$request.organisation}</dd>
            <dt>Фамилия имя отчество руководителя</dt>
            <dd>{$request.boss}</dd>
            {/if}

            {if $request.type == '2' || $request.type == '3'}
            <dt>Фамилия имя отчество  </dt>
            <dd>{$request.boss}</dd>
            {/if}
            
            {if $request.type == '1'}
            <dt>Юридический адрес </dt>
            {else}
            <dt>Адрес </dt>
            {/if}

            <dd>{$request.address_index}, {$request.address_region}, {$request.address_city}, ул. {$request.address_street} {$request.address_building_number}, {$request.address_office_number}</dd>
            <dt>Телефон: *</dt>
            <dd>{$request.phone}</dd>
            <dt>Электроная почта </dt>
            <dd>{$request.email}</dd>

            {if $request.type == '2' || $request.type == '3'}
            <dt>Паспорт: серия </dt>
            <dd>{$request.passport_ser}</dt>
            <dt>Паспорт: номер </dt>
            <dd>{$request.passport_nmbr}</dt>
            <dt>Паспорт: кем выдан </dt>
            <dd>{$request.passport}</dd>
            <dt>Паспорт: когда выдан </dt>
            <dd>{$request.passport_date}</dd>
            <dt>Паспорт: личный номер </dt>
            <dd>{$request.passport_personalnmbr}</dd>
            {/if}

            {if $request.type == '1' || $request.type == '2'}
            <dt>Регистрационный номер </dt>
            <dd>{$request.regnumber}</dd>
            <dd>№ решения о государственной регистрации *</dd>
            <dd>{$request.regnum}</dd>
            <dt>Орган осущeствивший регистрацию *</dd>
            <dd>{$request.regorg}</dd>
            <dd>Дата решения о государственной регистрации *</dd>
            <dd>{$request.regdate}</dd>
            {/if}
        </dl>        
    </div>
</div>

    

