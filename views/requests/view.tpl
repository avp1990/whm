<dl class="dl-horizontal dl-horizontal  col-md-8 col-md-offset-3">
    <dt>Информационный ресурс</dt>
    <dd>{$request.main_domain}</dd>
    <dt>Статус регистрации</dt>
    <dd>{$request.status}<br>Вы можете проверить информацию о регистрации [на сайте БелГИЭ]</dd>
</dl>
{include file="modules/addons/belgie//views/requests/parts/table.tpl"}
<div class="form-actions form-actions-client text-center">
    <input class="btn btn-blue btn-large" type="submit" name="registrant_submit" value="Изменить сведения" id="reg-input"/>(бесплатно)
</div>