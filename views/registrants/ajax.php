<div class="form-group fiz ur pred">
    <div class="col-sm-8">Собственник ресурса (регистрант) *</div>
</div>

<div class="form-group fiz ur pred">
    <label class="col-sm-4 control-label">Тип регистранта</label>
    <?php if($registrant['type'] == '1'): ?>
        <div div class="col-sm-8"><input name = "organization" class="form-control"  type="text" value="Юридическое лицо"> </div>
    <?php elseif($registrant['type'] == '2'): ?>
        <div div class="col-sm-8"><input name = "organization" class="form-control"  type="text" value="Индивидуальный предприниматель"> </div>
    <?php else:?>
        <div div class="col-sm-8"><input name = "organization" class="form-control"  type="text" value="Физическое лицо"> </div>
    <?php endif; ?>
</div>
            
<?php if($type  == '1'): ?>

    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Наименование Юридического лица * </label>
        <div div class="col-sm-8"><input name = "organization" class="form-control"  type="text" value="<?php echo $registrant['organization']; ?>"> </div>
    </div>

    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Фамилия имя отчество руководителя *</label>
        <div div class="col-sm-8"><input name = "boss" class="form-control"  type="text" value="<?php echo $registrant['boss']; ?>"> </div>
    </div>

<?php endif; ?>
  
<?php if($type == '3' || $type == '2'): ?>

    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Фамилия имя отчество * </label>
        <div div class="col-sm-8"><input name = "boss" class="form-control"  type="text" value="<?php echo $registrant['boss']; ?>"> </div>
    </div>

<?php endif; ?>

    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Населенный пункт *</label>
        <div div class="col-sm-8"><input name = "address_city" class="form-control"  type="text" value="<?php echo $registrant['address_city']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Область, район *</label>
        <div div class="col-sm-8"><input name = "address_region" class="form-control"  type="text" value="<?php echo $registrant['address_region']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Индекс *</label>
        <div div class="col-sm-8"><input name = "address_index" class="form-control"  type="text" value="<?php echo $registrant['address_index']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Улица *</label>
        <div div class="col-sm-8"><input name = "address_street" class="form-control"  type="text" value="<?php echo $registrant['address_street']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">№ Дома/корпус *</label>
        <div div class="col-sm-8"><input name = "address_building_number" class="form-control"  type="text" value="<?php echo $registrant['address_building_number']; ?>"> </div>
    </div>

<?php if($type == '1'): ?>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">№ Помещения * </label>
        <div div class="col-sm-8"><input name = "address_office_number" class="form-control"  type="text" value="<?php echo $registrant['address_office_number']; ?>"> </div>
    </div>
<?php else:?>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">№ квартиры (офиса) *</label>
        <div div class="col-sm-8"><input name = "address_office_number" class="form-control"  type="text" value="<?php echo $registrant['address_office_number']; ?>"> </div>
    </div>
<?php endif;?>

    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Контактный телефон * </label>
        <div div class="col-sm-8"><input name = "phone" class="form-control"  type="text" value="<?php echo $registrant['phone']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Электроная почта *</label>
        <div div class="col-sm-8"><input name = "email" class="form-control"  type="text" value="<?php echo $registrant['email']; ?>"> </div>
    </div>
  
<?php if($type == '2' || $type == '3'): ?>

    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Паспорт: серия </label>
        <div div class="col-sm-8"><input name = "passport_ser" class="form-control"  type="text" value="<?php echo $registrant['passport_ser']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Паспорт: номер *</label>
        <div div class="col-sm-8"><input name = "passport_nmbr" class="form-control"  type="text" value="<?php echo $registrant['passport_nmbr']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Паспорт: кем выдан *</label>
        <div div class="col-sm-8"><input name = "passport" class="form-control"  type="text" value="<?php echo $registrant['passport']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Паспорт: когда выдан *</label>
        <div div class="col-sm-8"><input name = "passport_date" class="form-control"  type="text" value="<?php echo $registrant['passport_date']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Паспорт: личный номер *</label>
        <div div class="col-sm-8"><input name = "passport_personalnmbr" class="form-control"  type="text" value="<?php echo $registrant['passport_personalnmbr']; ?>"> </div>
    </div>

<?php endif; ?>


<?php if($type == '1' || $type == '2'): ?>

    <div class="form-group fiz ur pred">
        <div class="col-sm-8">Регистрационная информация в ЕГР:</div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Регистрационный номер *</label>
        <div div class="col-sm-8"><input name = "regnumber" class="form-control"  type="text" value="<?php echo $registrant['regnumber']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">№ решения о государственной регистрации *</label>
        <div div class="col-sm-8"><input name = "regnum" class="form-control"  type="text" value="<?php echo $registrant['regnum']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Орган осущeствивший регистрацию *</label>
        <div div class="col-sm-8"><input name = "regorg" class="form-control"  type="text" value="<?php echo $registrant['regorg']; ?>"> </div>
    </div>
    <div class="form-group fiz ur pred">
        <label class="col-sm-4 control-label">Дата решения о государственной регистрации *</label>
        <div div class="col-sm-8"><input name = "regdate" class="form-control"  type="text" value="<?php echo $registrant['regdate']; ?>"> </div>
    </div>

<?php endif; ?>

<?php if ($registrant): ?>
    <div class="form-actions form-actions-client text-center">
        <input  class="btn btn-blue btn-large" type="button" value="Редактировать" id="reg-input"/>
    </div>
<?php endif; ?>
