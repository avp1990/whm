<table>
    <thead>
        <tr>
            <th>Доменное имя</th>
            <th>Владелец</th>
            <th>Статус</th>
        </tr>
    </thead>
    <tbody>
        {foreach from = $data_table item = i}
            <tr>
                <td><a href = "{$i->link}"> {$i->main_domain}</a></td>
                <td>{$i->boss}</td>
                <td>{$i->status}</td>
            </tr>
        {/foreach}
   </tbody>
</table>
