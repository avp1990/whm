<form role="form" id = "registrant_form" class="form-horizontal" action = "{$action_form_1}" method="POST" enctype="multipart/form-data">
    
    <input type="text" id = "id_registrant" name="id_registrant" value="" hidden>
    <input type="text" id = "id_resource" name="id_resource" value="{$id_resource}" hidden>
    
    <fieldset>

        <div class="form-group fiz ur pred">
                    <label class="col-sm-4 control-label">Доменное имя сайта * </label>
                    <div div class="col-sm-8"><input name = "domain_name" disabled class="form-control"  type="text" value="{$resource_name}"> </div>
        </div>

        <div class="form-group fiz ur pred">
                    <label class="col-sm-4 control-label">Дополнительные доменные имена, по которым открывается этот сайт </label>
                    <div div class="col-sm-6"><input class="form-control" type="text" name="add_names">Доменные имена, через пробел, без www </div>при наличии
        </div>
        
        <div class="form-group fiz ur pred">
                    <label class="col-sm-4 control-label">Описание сайта * </label>
                    <div div class="col-sm-8"><input class="form-control" type="text" name="description"></div>
        </div>

        <div class="form-group fiz ur pred">
            <label class="col-sm-4 control-label">Собственник ресурса (регистрант) *</label>
             <div div class="col-sm-8">
                 <select class="form-control" id ='registrant_dropdown_list' onchange="get_registrant()" required>
                     <option disabled selected>Выберите регистранта</option>
                     <option value="new">новый</option>
                     {foreach from = $registrants item = i}
                         <option value="{$i.id}">{$i.boss}</option>
                     {/foreach}
                   </select> 
             </div>
        </div>

        <div id = 'registrant_type' class="form-group fiz ur pred">
        </div>

        <div id = 'registrant'>
        </div>
                   
        <div class="form-actions form-actions-client text-center">
            <input onclick = "for_submit()" class="btn btn-blue btn-large" type="button" name="registrant_submit" value="Продолжить" id="reg-input"/>
        </div>
        
    </fieldset>
                   
</form>
                    
<script>
    
    function get_registrant() {
        
        var value = document.getElementById("registrant_dropdown_list");
        value = value.options[value.selectedIndex].value;
        
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
            document.getElementById("registrant_type").innerHTML = this.responseText;
            document.getElementById("registrant").innerHTML = '';
            
          }
        };
        var str = "?m=belgie?&r=RegistrantsController/ajax_choose_person/value=" + value;

        xhttp.open("GET", str , true);
        xhttp.send();
    }
    
    function get_registrant_type() {
        
        var value = document.getElementById("type");
        value = value.options[value.selectedIndex].value;
        
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
           document.getElementById("registrant").innerHTML = this.responseText;
          }
        };
        xhttp.open("GET", "?m=belgie?&r=RegistrantsController/ajax_choose_person_type/value=" + value, true);
        xhttp.send();
    }


function for_submit() {
    
    var select = $("#registrant_dropdown_list").find(":selected").val();

    if (select == 'new') {
        var qwe = $("#registrant_form").serializeArray();

        $.post("?m=belgie?&r=RegistrantsController/ajax_create", qwe ,
        function(data, status){
            $("#id_registrant").val(data);

        });
        

    } else {
            $("#id_registrant").val(select);
    }
    
    setTimeout(function(){
        $("#registrant_form").submit();
    }, 2000);
}

</script>
                  
             