<?php
use Illuminate\Database\Capsule\Manager as caps;

abstract class Controller {
    
    protected $view;

    public function __construct() {
        
        $this->view = new View();
 
    }
    
    public function render($templatefile, $vars = array(), $pagetitle = '', $breadcrumb = array(), $requirelogin = true, $forcessl = false) {
        return $this->view->render($templatefile, $vars, $pagetitle, $breadcrumb, $requirelogin, $forcessl);
    }
    
    public function render_ajax($templatefile, $vars = []) {
        return $this->view->render_ajax($templatefile, $vars);
    }
}