<?php

class Object {
    
    final public static function to_array($array) {
        
        foreach ($array as $key => $arr) {
            
            $array[$key] = (array)$arr;
            
        }
        
        return $array;
        
    }
    
    final public static function convert_array($value, $loadXml = FALSE)
    {
        // This is silly, but it works, and it works very well for what we are doing :)
        return json_decode(json_encode(($loadXml ? simplexml_load_string($value) : $value)), TRUE);
    }

}