<?php

class View {
    
    public function render($templatefile, $vars = array(), $pagetitle = '', $breadcrumb = array(), $requirelogin = true, $forcessl = false) {
        
        $path = VIEW_PATH . $templatefile;
        
        $vars['status'] = "module";
        
        return array(
            'pagetitle' => $pagetitle,
            'breadcrumb' => $breadcrumb,
            'templatefile' => $path,
            'requirelogin' => $requirelogin,
            'forcessl' => $forcessl,
            'vars' => $vars
        );
        
    }
    
    public function render_ajax($templatefile, $vars = []) {
        
        extract($vars);
        
        $path = MODULE_DIR . VIEW_PATH . $templatefile . '.php';
        
        include $path;
        
        exit();
        
    }

}