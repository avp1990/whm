<?php

use Illuminate\Database\Capsule\Manager as caps;

abstract class DBTableModel extends Model {
    
    protected $attributes = [];
    
    protected $table_name;
    
    /** @var Illuminate\Database\Query\Builder $table */
    protected $table;
    
    public $is_new;
            
    function __construct($data) {
        
        try {
            
            $this->attributes = $data;
            
            $this->is_new = true;

            $this->table_name = TABLE_PREFIX . strtolower(get_class($this));
            $this->table = caps::table($this->table_name);
            
            $this->validate();

        } catch (Exception $e) {
            throw new Exception("Can't create an object (" . static::class . ")" . $e->getMessage());
        }
        
    }

    public function validate() {

        if (!caps::schema()->hasTable($this->table_name)) {
            throw new Exception('Validation method of ' . static::class . "cant' find table in db (modlel clas must be the same name as its table)");
        }
       
        $columns = static::getColumnListing();
        $columns = array_flip($columns);
        
        $diff = array_diff_key($columns, $this->attributes);
        
        if (!empty($diff)) {
            throw new Exception('Validation method of ' . static::class . ' incorrect amount of arguments, diffrence: ' . $diff);
        }
        
        return true;

    }
    
    public function get_summary () {
        return $this->attributes;
    }
    
    public function rules () {
        return [];
    }

    public function save() {
        
        try {
            
            $this->validate();

            if ($this->is_new) {

                $this->is_new = false;

                $this->id = $this->table->insertGetId($this->attributes);

                return $this->id;

            } else {

                $this->table->where('id', $this->id)->update($this->attributes);

                return $this->id;

            }
            
        } catch (Exception $ex) {
            throw new Exception("Can't save an object (" . static::class . "), incorrect set of attributes");
        }

    }

    public function __set($name, $value) {
        
        if (array_key_exists($name, $this->attributes)) {
            $this->attributes[$name] = $value;
        }
        
    }
    
    public function __get($name) {
        
        if (array_key_exists($name, $this->attributes)) {
            return $this->attributes[$name];
        }
        
    }
    
    public static function find_by_id($id, $to_array = true) {
        
        if(!is_numeric($id) || !is_bool($to_array)) {
            throw new Exception(static::class . ' ' .  __METHOD__ . ' incorect arguments');
        }
        
        $table_name = TABLE_PREFIX . strtolower(static::class);
        
        $result = caps::table($table_name)->find($id);

        if($to_array){
            $result = Object::convert_array($result);
        }

        return $result;
        
    }
    
    public static function getColumnListing() {
        
        $table_name = TABLE_PREFIX . strtolower(static::class);
        return caps::schema()->getColumnListing($table_name);
        
    }
    
    abstract public static function get_instanse_by_id($id);

}
