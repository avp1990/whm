<?php

class Router {
    
    public static function start() {

        $r = $_GET['r'];
       
        $url = explode('/', $r);

        $class = $url[0];
        $method = $url[1];
        unset($url[0]);
        unset($url[1]);
        
        $params = array();
        
        if (!empty($_POST)) {
            $params[] = $_POST;
        }
        
        foreach ($url as $row) {

            $temp = explode('=', $row);
            $params[$temp[0]] = $temp[1];

        }

        include MODULE_DIR . "/controllers/" . $class. ".php";
        
        $controller = new $class();

        try {
            $page = call_user_func_array(array($controller, $method), $params);
        } catch(Exception $ex) {
            echo $ex->getMessage();
        }
        
        return $page;

    }
    
}