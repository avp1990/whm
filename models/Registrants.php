<?php

use Illuminate\Database\Capsule\Manager as caps;

class Registrants extends DBTableModel {

    public static function get_instanse_by_id($id) {

        $obj = new Registrants(static::find_by_id($id));

        $obj->is_new = false;
        
        return $obj;
        
    }

}

    
    /*
     * 
    
    public function attribute_labels() {
        
        return [
            'id' => 'ID',
            'id_acc' => 'Brand',
            'type' => 'Price',
            'organization' => 'Price',
            'boss' => 'Price',
            'address_city' => 'Price',
            'address_region' => 'Price',
            'address_index' => 'Price',
            'address_street' => 'Price',
            'address_building_number' => 'Price',
            'address_office_number' => 'Price',
            'phone' => 'Price',
            'email' => 'Price',
            'passport_ser' => 'Price',
            'passport_nmbr' => 'Price',
            'passport' => 'Price',
            'passport_date' => 'Price',
            'passport_personalnmbr' => 'Price',
            'regnumber' => 'Price',
            'regnum' => 'Price',
            'regorg' => 'Price',
            'regdate' => 'Price',
        ];
        
    }
    
    public function rules()
    {
        return [
            [['id', 'brand', 'price'], 'required'],
            [['id', 'brand', 'price'], 'integer'],
        ];
    }
     */