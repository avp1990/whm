<?php

use Illuminate\Database\Capsule\Manager as caps;

require_once MODULE_DIR . "/models/Resources.php";
require_once MODULE_DIR . "/models/Registrants.php";

class Requests  extends DBTableModel {
    
    private $resource;
    private $registrant;
    
    public function __construct(Resources $resource, Registrants $registrant) {

        $this->resource = $resource;
        $this->registrant = $registrant;
        
        $row = array_merge($this->registrant->get_summary(),$this->resource->get_summary());
        
        $row['id'] = '';
        $row['id_resourse'] = $resource->id;
        $row['id_registrant'] = $registrant->id;
        $row['status'] = 'требует отправки';
        unset($row['req']);
        
        $columns = Requests::getColumnListing();
        $columns = array_flip($columns);
        

        parent::__construct(array_intersect_key($row, $columns));

    }
    
    public static function get_instanse_by_id($id) {
        
        $data = static::find_by_id($id);

        if (empty($data)) {
            throw new Exception('Cant instantiate by id');
        }
        
        $resource = Resources::get_instanse_by_id($data['id_resourse']);
        $registrant = Registrants::get_instanse_by_id($data['id_registrant']);
        
        $obj = new Requests($resource, $registrant);
        $obj->is_new = false;

        return $obj;
        
    }
    

    
    
}
