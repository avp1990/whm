<?php
use Illuminate\Database\Capsule\Manager as caps;

class Resources  extends DBTableModel {
    
    private $registrant;
    
    public function add_registrant(Registrants $registrant) {
        $this->registrant = $registrant;
    }
    
    public function change_status() {
        
    }
    
    public function register(Requests $request) {
        $this->id_request = $request->id;
    }

    public static function get_instanse_by_id($id) {

        $obj = new Resources(static::find_by_id($id));
        $obj->is_new = false;
        
        return $obj;
    }

}
