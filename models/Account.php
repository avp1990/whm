<?php
use Illuminate\Database\Capsule\Manager as caps;

class Account extends Model {
    
    private $id;
    private $resources;
    
    public function __construct($id) {
        
        $this->id = $id;
        $this->resources = caps::table('belgie_modul_resources');

    }
    
    public function get_registrants($to_array = true) {

        $result = caps::table(TABLE_PREFIX . strtolower('registrants'))->where('id_acc', $this->id)->get();

        if ($to_array) {
            $result = Object::convert_array($result);
        }

        return $result;

    }
    
    public function get_users_by_server(Server $server) {
        
        $server_id = $server->get_id();
        
        $users = caps::table('tblhosting')
                ->leftJoin('tblproducts', 'tblproducts.id', '=', 'tblhosting.packageid')
                ->select('tblhosting.domain', 'tblhosting.server', 'tblhosting.username', 'tblproducts.name as product_name','tblhosting.dedicatedip','tblhosting.domainstatus')
                ->where('tblhosting.userid', $this->id)
                ->where('tblhosting.server', $server_id)
                ->get();
        
        $users = Object::to_array($users);
        
        foreach ($users as $key => $user) {
            
            if (in_array($user['domainstatus'], config['ommit_if_status_is'])) {
                unset($users[$key]);
            }
            
        }
        
        return $users;
        
    }
    
    public function get_resources() {
        
        $all_user_resources = $this->resources
                ->leftJoin('belgie_modul_requests', 'belgie_modul_resources.id_request', '=', 'belgie_modul_requests.id')
                ->select('belgie_modul_resources.*', 'belgie_modul_requests.boss','belgie_modul_requests.status')
                ->where('belgie_modul_resources.id_acc', $this->id)
                ->get();

        $all_user_resources = $this->define_links($all_user_resources);
        
        return $all_user_resources;
    }
    
    private function define_links($all_user_resources) {
        
        $url = '?m=belgie?&r=RequestsController/action_view/id_recuest=';

        
        foreach ($all_user_resources as $key => $row) {
            
            if ($row->id_request) {
                $url = '?m=belgie?&r=RequestsController/action_view/id_recuest=' . $row->id_request;
            } else {
                $url = '?m=belgie?&r=ResourcesController/action_view/id_resource=' . $row->id;
            }
            
            $all_user_resources[$key]->link =  $url;
            
        }
        
        return $all_user_resources;
        
    }
    
    public function refresh($all_users_domens) {

        //получить текущие данные о ресурсах пользователя
        $resources = caps::table('belgie_modul_resources')->where('belgie_modul_resources.id_acc' , $_SESSION['uid'])->get();

        $resources = Object::to_array($resources);

        //обновляем таблицу

        $this->synchronization_one($resources, $all_users_domens);
        $this->synchronization_two($all_users_domens, $resources);
        $this->synchronization_three($all_users_domens, $resources);

    }
    
    private function synchronization_one(array $resources_db, array $resources_server) {

        $domains = array_column($resources_db, 'domain');
        
        ////There is in table, there is no in cpanel
        $result = array_diff($domains, $resources_server);
        
        if ($result) {
            caps::table('belgie_modul_resources')
                ->whereIn('domain', $result)
                ->update(['id_acc' => '1']);
        }
        
    }
    
    private function synchronization_two(array $resources_server, array $resources_db) {
        
        $domains = array_column($resources_db, 'domain');
        
        ////There is in table, there is no in cpanel
        $result = array_diff($resources_server, $domains);
        
        foreach($result as $res) {
            
            caps::table('belgie_modul_resources')->insert(
                array('id_acc' => $_SESSION['uid'], 'domain' => $res, 'status' => 'не зарегистрирован')
            );
            
        }
        
    }
    
    private function synchronization_three(array $resources_server, array $resources_db) {
        
        $domains = array_column($resources_db, 'domain');
        
        ////There is in table, there is no in cpanel
        $result = array_diff($resources_server, $domains);
        
        $result = array_keys($result);
        
        $result = array_flip($result);
        
        $intersect = array_intersect_key($resources_db, $result);
        
        foreach ($intersect as $row) {
            
            if ($row['id_acc'] != $_SESSION['uid']) {

                caps::table('belgie_modul_resources')
                    ->where('id', $domains_table['id'])
                    ->update(['id_acc' => $_SESSION['uid']]);

            }
            
        }

    }
    
}
