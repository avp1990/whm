<?php

class Cpanel extends Server {
    
    public function __construct($server_config) {
        
        $this->id = $server_config['id'];
        $this->login = $server_config['login'];
        $this->password = $server_config['pass'];
        $this->host = $server_config['host'];
        $this->ip = $server_config['ip'];
        
    }
    
    public function get_id() {
        return parent::get_id();
    }
    
    public function execute($array) {
        
        $request = $array['request'];
        
        $cleanaccesshash = preg_replace( "'(\n|\n)'", "", $this->password );

	$authstr = "WHM " . $this->login . ":" . $cleanaccesshash;

	$ch = curl_init();

	curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
	curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
	curl_setopt( $ch, CURLOPT_URL, $this->host . $request );

	curl_setopt( $ch, CURLOPT_HEADER, 0 );
	curl_setopt( $ch, CURLOPT_RETURNTRANSFER, 1 );
	$curlheaders[0] = "Authorization: " . $authstr;

	curl_setopt( $ch, CURLOPT_HTTPHEADER, $curlheaders );
	curl_setopt( $ch, CURLOPT_TIMEOUT, 400 );

	$data = curl_exec( $ch );

	$data = json_decode($data, true);
	curl_close( $ch );
        
	return $data;
        
    }
  
    public function get_user_main_domain ($username) {
        
        $cpanelrequest = array('request' => "/json-api/cpanel?cpanel_jsonapi_user=" . $username . "&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=DomainLookup&cpanel_jsonapi_func=getmaindomain");
        $data = $this->execute($cpanelrequest);
        
        if (isset($data['cpanelresult']['error'])) {
            
            $message = $data['cpanelresult']['error'];
            
            throw new Exception("SERVER ID:{$this->get_id()} get_user_main_domain Fault: (Message: {$message})");
            
        }
        
        if(isset($data['cpanelresult']['data'][0]['main_domain'])) {
            return $data['cpanelresult']['data'][0]['main_domain'];
        }
        
    }
    
    public function get_all_add_domains() {
        
        $cpanelrequest = array('request' => "/json-api/convert_addon_list_addon_domains?api.version=1");
        return $this->execute($cpanelrequest);
        
    }
    
    public function get_all_user_add_domains($username) {
        
        $cpanelrequest = array('request' => "/json-api/cpanel?cpanel_jsonapi_user=" . $username . "&cpanel_jsonapi_apiversion=2&cpanel_jsonapi_module=AddonDomain&cpanel_jsonapi_func=listaddondomains");
        
        $data = $this->execute($cpanelrequest);
        
        if (isset($data['cpanelresult']['error'])) {
            
            $message = $data['cpanelresult']['error'];
            
            throw new Exception("SERVER ID:{$this->get_id()} get_all_user_add_domains Fault: (Message: {$message})");
            
        }
        
        $data = $data['cpanelresult']['data'];
        
        $domains = array();
        
        foreach ($data as $row) {
            $domains[] = $row['domain'];
        }
        
        return $domains;

    }
    
    public function get_all_add_domains_active() {
        
        $all_domains = $this->get_all_add_domains();
        
        foreach ($all_domains as $key => $domain) {

            if($this->check_ip($domain, $ip)) {
                unset($all_domains['$key']);
            }
            
        }
        
        return $all_domains;
        
    }

    
    public function get_all_add_domains_user_active($username) {
        
        $all_domains = $this->get_all_add_domains_user($username);
        
        foreach ($all_domains as $key => $domain) {

            if($this->check_ip($domain, $ip)) {
                unset($all_domains['$key']);
            }
            
        }
        
        return $all_domains;
        
    }

}