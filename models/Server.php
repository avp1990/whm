<?php

abstract class Server extends Object {
    
    protected $id;
    protected $login;
    protected $password;
    protected $host;
    protected $ip;

    public function get_id() {
        
        return $this->id;
        
    }
    
    abstract protected function execute($array);
    
    abstract protected function get_user_main_domain ($username);
    
    abstract protected function get_all_add_domains();
    abstract protected function get_all_user_add_domains($username);
    
    abstract protected function get_all_add_domains_active();
    abstract protected function get_all_add_domains_user_active($username);

    protected function check_ip($domain, $ip) {
        
        $ips = gethostbynamel($domain);
        
        if (array_search($ip, $ips)) {
            
            return false;
            
        }
        
        return true;
        
    }
    
    public function get_domains_of_user_set($users_set) {
        
        $result = [];
        $temp = [];
        
        foreach ($users_set as $user) {
            
            $result = array_merge($result, $this->get_all_user_add_domains($user['username']));
            
            if ($this->get_user_main_domain($user['username'])) {
                $result[] = $this->get_user_main_domain($user['username']);
            }
  
        }
        
        return $result;
        
    }
}