<?php

class SolidCP extends Server
{
    private $_port;
    private $_secured;
    private $_caching;
    private $_compression;
        
    function __construct($config) {
            
        $port = 9002;
        $secured = FALSE;
        $caching = FALSE;
        $compression = TRUE;

        $this->id = $config['id'];
        $this->login = $config['login'];
        $this->password = $config['pass'];
        $this->host = $config['host'];
        $this->_port = $port;
        $this->_secured = $secured;
        $this->_caching = $caching;
        $this->_compression = $compression;
            
    }
    
    public function get_id() {
        return parent::get_id();
    }

        public function execute($array) {
            
        $service = $array['service'];
        $method = $array['method'];
        $params = $array['params'];
           
        // Set the Enterprise Server full URL
        $host = (($this->_secured) ? 'https' : 'http') . "://{$this->host}:{$this->_port}/{$service}?WSDL";
        
        try {
            
            // Create the SoapClient
            $client = new SoapClient($host, array('login' => $this->login, 'password' => $this->password, 'compression' => (($this->_compression) ? (SOAP_COMPRESSION_ACCEPT | SOAP_COMPRESSION_GZIP) : ''), 'cache_wsdl' => ($this->_caching) ? 1 : 0));
			
            // Execute the request and process the results
            return call_user_func(array($client, $method), $params);
            
        }
        catch (SoapFault $e) {
            throw new Exception("SOAP Fault: (Code: {$e->getCode()}, Message: {$e->getMessage()})");
        }
        catch (Exception $e) {
            throw new Exception("General Fault: (Code: {$e->getCode()}, Message: {$e->getMessage()})");
        }
    }
    
    public function get_user_main_domain ($username) {
        return false;    
    }

    public function get_all_add_domains() {}
        
    public function get_all_user_add_domains($username) {

        $UserId = $this->getUserByUsername($username);
        $UserId = $UserId['UserId'];

        $packages = $this->getUserPackages($UserId);

        $domains = array();

        if(empty($packages['0'])) {
            
            $temp = $packages;
            $packages = array();

            $packages[] = $temp;

        }

        foreach ($packages as $pack) {

            $temp = $this->getDomains($pack['PackageId']);
            $temp = $temp['DomainInfo'];

            foreach ($temp as $row) {

               if (isset($row['WebSiteName'])) {
                   
                    $domains[$row['WebSiteName']] = $row['WebSiteName'];
                    break;
                    
               }


            }

        }

        return $domains;
            
    }

    public function get_all_add_domains_active() {}
    public function get_all_add_domains_user_active($username) {}
        
    public function getUserByUsername($username) {
            
        try {
            return Object::convert_array($this->execute(array('service' => 'esUsers.asmx', 'method' => 'GetUserByUsername', 'params' => array('username' => $username)))->GetUserByUsernameResult);
        }
        catch (Exception $e) {
                throw new Exception("GetUserByUsername Fault: (Code: {$e->getCode()}, Message: {$e->getMessage()})", $e->getCode(), $e);
        }
                
    }

    public function getUserPackages($userId) {
            
        try {
                return Object::convert_array($this->execute(array('service' => 'esPackages.asmx', 'method' => 'GetMyPackages', 'params' => array('userId' => $userId)))->GetMyPackagesResult->PackageInfo);
        }
        catch (Exception $e) {
                throw new Exception("GetMyPackages Fault: (Code: {$e->getCode()}, Message: {$e->getMessage()}", $e->getCode(), $e);
        }
    }

    public function getDomains($packageId) {
            
        try {
            return Object::convert_array($this->execute(array('service' => 'esServers.asmx', 'method' => 'GetDomains', 'params' => array('packageId' => $packageId)))->GetDomainsResult);
        }
        catch (Exception $e) {
                throw new Exception("GetDomains Fault: (Code: {$e->getCode()}, Message: {$e->getMessage()})", $e->getCode(), $e);
        }
                
    }
        
    public function GetUsers($ownerId, $recursive = true) {
            
        try {
            return Object::convert_array($this->execute(array('service' => 'esUsers.asmx', 'method' => 'GetUsers', 'params' => array('ownerId' => $ownerId, 'recursive' => $recursive)))->GetUsersResult);
        }
        catch (Exception $e) {
                throw new Exception("GetDomains Fault: (Code: {$e->getCode()}, Message: {$e->getMessage()})", $e->getCode(), $e);
        }
                
    }
        
    public function getUserDomainsPaged($userId, $filterColumn = NULL, $filterValue = NULL, $sortColumn = NULL, $startRow = 0,$maximumRows = 100) {
            
        $array = array(
            'userId' => $userId,
            'startRow' => $startRow,
            'maximumRows' => $maximumRows,
        );

        try{
            return $this->execute(array('service' => 'esUsers.asmx', 'method' => 'GetUserDomainsPaged', 'params' => $array))->GetUserDomainsPagedResult;
        }
        catch (Exception $e){
            throw new Exception("GetDomains Fault: (Code: {$e->getCode()}, Message: {$e->getMessage()})", $e->getCode(), $e);
        }
    }
     
}