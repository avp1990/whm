<?php
use WHMCS\ClientArea;
use Illuminate\Database\Capsule\Manager as caps;

define('CLIENTAREA', true);
define('MODULE_DIR', __DIR__);
define('TABLE_PREFIX', 'belgie_modul_');
define('VIEW_PATH', '/views/');



require_once '/home/user1001/public_html/init.php';



require_once "core/Object.php";
require_once "core/Router.php";
require_once "core/Controller.php";
require_once "core/Model.php";
require_once "core/DBTableModel.php";
require_once "core/View.php";
require_once MODULE_DIR . "/models/Resources.php";


function belgie_config() {

    $configarray = array(
    "name" => "Belgie",
    "description" => "It allows send applicatins",
    "version" => "1.0",
    "author" => "Papilov",
    "fields" => array());
    return $configarray;

}

function belgie_activate() {

    return array('status'=>'info','description'=>'Module "Belgie" was activated succesfull.');

}

function belgie_deactivate() {

    return array('status'=>'info','description'=>'Module "Belgie" was diactivated succesfull.');

}

function belgie_output($vars) {

	$modulelink = $vars['modulelink'];
	$version = $vars['version'];

	$option1 = $vars['option1'];
	$option2 = $vars['option2'];
	$option3 = $vars['option3'];
	$option4 = $vars['option4'];
	$option5 = $vars['option5'];
	$option6 = $vars['option6'];
	$LANG = $vars['_lang'];

        echo '<p>Belgie</p>';
        echo '<p>It allows send applicatins</p>';

}

function belgie_clientarea($vars) {

    $modulelink = $vars['modulelink'];
    $version = $vars['version'];
    $option1 = $vars['option1'];
    $option2 = $vars['option2'];
    $option3 = $vars['option3'];
    $option4 = $vars['option4'];
    $option5 = $vars['option5'];
    $option6 = $vars['option6'];
    $LANG = $vars['_lang'];
    
    
    
    return Router::start();
   
}


/*
 *     return array(
            'pagetitle' => 'Addon Module',
            'breadcrumb' => array('index.php?m=demo'=>'Demo Addon'),
            'templatefile' => 'testtemplate',
            'requirelogin' => true, # accepts true/false
            'forcessl' => false, # accepts true/false
            'vars' => array(
                'testvar' => 'demo',
                'anothervar' => 'value',
                'status' => 'module',
            ),
        );
 */