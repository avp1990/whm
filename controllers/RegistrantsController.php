<?php
use Illuminate\Database\Capsule\Manager as caps;
require_once MODULE_DIR . "/models/Registrants.php";

class RegistrantsController extends Controller {
    
    public function ajax_create($data) {

        try {
        
            $columns = Registrants::getColumnListing();
            $columns = array_flip($columns);

            $data = array_intersect_key($data,$columns);

            $registrant = new Registrants($data);

            $registrant->id_acc = $_SESSION['uid'];

            $id = $registrant->save();

            return $this->render_ajax('registrants/ajax_create', ['id' => $id]);
        
        } catch (Exception $e) {
            
            return $this->render('error', ['message' => $e->getMessage()]);
                        
        }
        
    }
    
    public function ajax_choose_person($value) {
        
        try {

            if ($value == 'new') {
                return $this->render_ajax('registrants/ajax_registrant_type');
            }

            if (is_numeric($value)) {

                $registrant = Registrants::get_instanse_by_id($value);

                $type = $registrant->type;

                return $this->render_ajax('registrants/ajax', ['registrant' => $registrant->get_summary(), 'type' => $type]);

            }
        
        } catch (Exception $e) {
            
            return $this->render('error', ['message' => $e->getMessage()]);
            
        }
     
    }
    
    public function ajax_choose_person_type($type) {
        
             return $this->render_ajax('registrants/ajax',['type' => $type]);        

    }
    
}