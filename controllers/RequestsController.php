<?php

use Illuminate\Database\Capsule\Manager as caps;

require_once MODULE_DIR . "/models/Account.php";
require_once MODULE_DIR . "/models/Server.php";
require_once MODULE_DIR . "/models/SolidCP.php";
require_once MODULE_DIR . "/models/Cpanel.php";
require_once MODULE_DIR . "/models/Resources.php";
require_once MODULE_DIR . "/models/Registrants.php";
require_once MODULE_DIR . "/models/Requests.php";
require_once MODULE_DIR . "/models/Requests.php";

class RequestsController extends Controller {
    

    
    public function action_view($id_request) {
        
        try {

            // $registrant = Registrants::get_instanse_by_id(12);

             //print_r($registrant->get_summary());


             $request = Requests::get_instanse_by_id($id_request);
             $templatefile = 'requests/view';

             $vars = ['request' => $request->get_summary()];


             return $this->render($templatefile, $vars);
             
        } catch (Exception $e) {
            
            return $this->render('error', ['message' => $e->getMessage()]);
            
        }

    }
    
    public function action_status_not_necessary($id_resource) {
        
        $templatefile = 'not_necessary';
        
        $vars = array();
        
        return $this->view->render($templatefile);
        
    }
    
}
    
/*

    

    ( [id] => 44 [id_acc] => 2 [type] => [organization] => Физическое лицо [boss] => [address_city] => [address_region] => 
    [address_index] => [address_street] => [address_building_number] => 
    [address_office_number] => [phone] => [email] => [passport_ser] => [passport_nmbr] => 
    [passport] => [passport_date] => [passport_personalnmbr] => [regnumber] => [regnum] => [regorg] => 
    [regdate] => [id_registrant] => 0 [status] => не зарегистрирован 
    [domain] => help.by2.by [req] => 0 [ips] => [port] => ) 
}


*/



/*
 *     private function create_xml($arrSearchReg, $arrReplaceReg, $template_path, $put_path, $new_file_name) {
        
        $xml_reg_templates = file_get_contents($template_path);
        $xml_reg_files = str_replace($arrSearchReg, $arrReplaceReg, $xml_reg_templates);
        file_put_contents($put_path. $new_file_name , $xml_reg_files);
        
    }
 * 
 *     public function action_send_request() {

        $this->create_xml($arrSearchReg, $arrReplaceReg, $template_path, $put_path, $new_file_name);
        
        $this->create_xml($arrSearchReg, $arrReplaceReg, $template_path, $put_path, $new_file_name);

        return $this->render('send_request');
    }
 */