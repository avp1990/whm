<?php
use Illuminate\Database\Capsule\Manager as caps;

require_once MODULE_DIR . "/models/Account.php";
require_once MODULE_DIR . "/models/Server.php";
require_once MODULE_DIR . "/models/SolidCP.php";
require_once MODULE_DIR . "/models/Cpanel.php";
require_once MODULE_DIR . "/models/Resources.php";
require_once MODULE_DIR . "/models/Registrants.php";
require_once MODULE_DIR . "/models/Requests.php";

class ResourcesController extends Controller {

    public function action_view($id_resource) {
        echo 1;
        try {
        
        $templatefile = 'resources/not_registred';

        $vars = array(

            'registr' => '?m=belgie?&r=ResourcesController/page_register/id=' . $id_resource,
            'not_necessary' => '?m=belgie?&r=ResourcesController/action_status_not_necessary/id=' . $id_resource,

        );

        return $this->render($templatefile, $vars);
        
        } catch (Exception $e) {
            
            return $this->render('error', ['message' => $e->getMessage()]);
            
        }

    }
    
    public function action_list () {

        try {
        
            $actual_domains = [];

            $config = require(MODULE_DIR . '/config.php');

            $servers = [];

            $servers[] = new Cpanel($config['servers']['v01']);
            $servers[] = new Cpanel($config['servers']['v03']);
            $servers[] = new Cpanel($config['servers']['v05']);
            $servers[] = new SolidCP($config['servers']['v02']);       

            $acc = new Account($_SESSION['uid']);

            //обновляем таблицу ресурсов
            $acc->refresh($actual_domains);

           $all_user_resources = $acc->get_resources();

           return $this->render('resources/list', ['data_table' => $all_user_resources]);
       
       } catch (Exception $e) {
           
            return $this->render('error', ['message' => $e->getMessage()]);
                       
       }

    }
    
    public function page_register($id_resource) {
        
        try {
        
            $resource = Resources::get_instanse_by_id($id_resource);

            $acc = new Account($_SESSION['uid']);

            $registrants = $acc->get_registrants();

            $templatefile = 'resources/page_register';

            $vars = array(

                'action_form_1' => '?m=belgie?&r=ResourcesController/register_resource',
                'resource_name' => $resource->main_domain,
                'id_resource' => $id_resource,
                'registrants' => $registrants,

            );
            
            return $this->render($templatefile, $vars);
            
        } catch (Exception $e) {
            
            return $this->render('error', ['message' => $e->getMessage()]);
            
        }


        
    }
    
    public function register_resource($data) {
        
        try { 
            
            $resource = Resources::get_instanse_by_id($data['id_resource']);
            $registrant = Registrants::get_instanse_by_id($data['id_registrant']);

            $resource->description = $data['description'];
            $resource->domains = $data['add_names'];

            $request = new Requests($resource, $registrant);

            $request->save();

            $resource->register($request);

            $resource->save();

            $vars = array(

                'request' => $request->get_summary(),
                'action_form_1' => '?m=belgie?&r=RequestsController/action_send_request',
                'id_resource' => $id_resource,

            );
            
            return $this->render('resources/create_request', $vars);

        } catch (Exception $e) {
            
            return $this->render('error', ['message' => $e->getMessage()]);
            
        }
        
        
        
    }
    
    public function change_status_not_nessesary(){}
    

}

